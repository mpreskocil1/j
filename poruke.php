<?php
$login_err   = '{"h_message":"login redirect","h_errcode":999}';
$request_err = '{"h_message":"request error","h_errcode":998}';
$procedure_err  = '{"h_message":"method error","h_errcode":997}';
$projekt_err  = '{"h_message":"project error","h_errcode":996}';
$logout  = '{"h_message":"Uspješno ste odjavljeni","h_errcode":0}';
$wrong_login = '{"h_message":"Pogrešno korisničko ime ili zaporka. Molimo pokušajte ponovno","h_errcode":111}';
$mandatory_field = '{"h_message":"Polje #polje je obvezni podatka. Molimo pokušajte ponovno","h_errcode":112}';
?>