<?php 
error_reporting(0);
header('Access-Control-Allow-Origin:http://localhost');
header('Access-Control-Allow-Credentials: true');
//header('Content-Type: application/json');
?>
<?php


require_once('database.php');
session_start(); 

//dozvoljavam samo POST i GET metodu u svakom slučaju prepunjava se $injson varijabla
switch ($_SERVER['REQUEST_METHOD']) {
    case 'POST':
        $injson = json_encode($_POST);
        break;
    case 'GET':
        $injson = json_encode($_GET);
        break;
    default:
        echo $request_err;
        return;
}

$in_obj = json_decode($injson);

//provjera podataka koji su došli s inputa
//print_r($in_obj);

//logout, pozivam prije konekcije na bazu jer je ne trebam, radim samo sa SESSIONom na serveru
if($in_obj->procedura =="p_logout"){
    session_destroy();
    echo $logout;
    return; 
}

//ako session nije kreiran samo p_login mogu zvati
if (!isset( $_SESSION['ID']) && $in_obj->procedura !="p_login") {
       echo $login_err;
       return; 
}

//refresh, vraćam podatke iz SESSIONa i to napravim prije konekcije na bazu, ne treba mi baza za ovo
if (isset($_SESSION['ID']) && $in_obj->procedura == "p_refresh") {
    echo json_encode($_SESSION);
    return; 
}

//konekcija na bazu

try{
    $db = f_get_database();
    }catch (Exception $e){
    echo $database_error;
    return;
    }
   
//raspoređujem pozive prema funkcijama
switch ($in_obj->procedura) {
    case 'p_login':
        f_login($db, $in_obj);
        break;
    case 'p_get_poduzeca':
        f_get_poduzeca($db, $in_obj);
        break;
    case 'p_get_partneri':
        f_get_partneri($db, $in_obj);
        break;
    case 'p_get_korisnici':
        f_get_korisniki($db, $in_obj);
        break;
    case 'p_get_skladista':
        f_get_skladista($db, $in_obj);
        break;
    case 'p_get_temeljnica':
        f_get_temeljnica($db, $in_obj);
        break;
    case 'p_get_konta':
        f_get_konta($db, $in_obj);
        break;
    case 'p_get_poste':
        f_get_poste($db, $in_obj);
        break;
    case 'p_get_porezneopcine':
        f_get_porezneopcine($db, $in_obj);
        break;
    case 'p_get_porezi':
        f_get_porezi($db, $in_obj);
        break;
    case 'p_get_doprinosi':
        f_get_doprinosi($db, $in_obj);
        break;
    case 'p_get_banke':
        f_get_banke($db, $in_obj);
        break;
    case 'p_save':
         $korisnici = New Korisnici();
         $korisnici = $korisnici->checkPrepare($db, $injson, "korisnici");
         $korisnici->save();
         echo $korisnici->action;
         print_r($korisnici->redak);
         break;    
    default:
        echo $request_err;
        return;
}


?>