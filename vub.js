var url = "https://dev.vub.zone/sandbox/router.php";

function showTable(inJson) {
    fakeJson = JSON.stringify(inJson).replace(/\"/g, "~");
    var inData = { "projekt": inJson.projekt, "procedura": inJson.procedura.get, "perPage": inJson.perPage, "page": inJson.page }
    var hkeys = Object.keys(inJson.columns.ID);
    var hvalues = Object.values(inJson.columns.ID);
    //console.log('hkeys=' + hkeys);
    //console.log('hvalues=' + hvalues);
    if (inJson.insert) {
        var tablica = '<br><button type="button" style="float:right;" class="btn btn-success" onclick="insertRow(\'' + fakeJson + '\')">Insert <i class="fa fa-download" aria-hidden="true"></i></button><br><br>';
    } else {
        tablica = '<br><br><br>';
    }

    tablica += '<table class="table table-hover"><tbody><thead><tr>';

    $.each(inJson.columns, function (key, value) {
        if(value.show){
            if (value.alias){
                tablica += '<th scope="col">' + value.alias + '</th>';
            }else{
                tablica += '<th scope="col">' + key + '</th>';
            }
            
        }
    });
  
    if (inJson.action) {
        tablica += '<th scope="col">ACTION</th>';
    }

    if (inJson.page == null || inJson.page == "") {
        inJson.page = 1;
    }
    console.log(inJson);
    $.ajax({
        type: 'POST',
        url: url,
        data: inData,
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            var count = jsonBody.count;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (ke, va) {
                    tablica += '<tr>';
                    $.each(inJson.columns, function (k, v) {
                        if(v.show){
                          tablica += '<td>' + jsonBody.data[ke][k] + '</td>';
                       }
                    });
                    if (inJson.action) {
                        inJson.ID = va.ID; 
                        v_json = JSON.stringify(inJson).replace(/\"/g, "~");
                        tablica += '<td>';
                        if(inJson.edit){
                           tablica += '<button type="button" class="btn btn-primary" onclick="showRow(\'' + v_json + '\')">Edit <i class="fas fa-edit"></i></button> ';
                        }
                        if(inJson.delete){
                           tablica += '<button type="button" class="btn btn-danger" onclick="delKorisnik(' + v_json + ')">Delete <i class="far fa-trash-alt"></i></button>';
                        }   
                        tablica += '</td>';
                    }
                    tablica += '</tr>';
                });
                tablica += '</tbody></table>';
                tablica += pagination(inJson.page, inJson.perPage, count);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: false

    });
    if(inJson.where){
        $(inJson.where).html(tablica); 
    }else{
        return tablica;
    } 
}


function showRow(inJson) {
    fakeJson = inJson;
    inJson = JSON.parse(inJson.replace(/~/g, "\""));
    console.log('Pogledaj json' + fakeJson);

    var tablica = '<table class="table table-hover" id="promjena"><tbody>';
    $.ajax({
        type: 'POST',
        url: url,
        data: {"projekt": inJson.projekt, "procedura": inJson.procedura.get, "ID": inJson.ID},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    
                    $.each(inJson.columns, function (key, value) { 
                       if (key == 'ID') {
                          tablica += '<tr><th scope="col">' + ' '/* key*/ + '</th><td><input type="text" id="' + key +  '"value="' + jsonBody.data[k][key] + '" hidden></td></tr>';
                       }else{
                           if (value.function){
                               var funkcija = value.function  + '(' + jsonBody.data[k][key] + ',\'' + key + '\'' + ',' + value.lov + ')';
                               tablica += '<tr><th scope="col">' + key + '</th><td>' + eval(funkcija) + '</td></tr>'; 
                           }else{
                               if(value.type){
                                   if(value.hidden){
                                    var spojen = '<tr><th scope="col">' + ' ' /* key*/ + '</th><td><input type="text" id="' + key +  '"value="' + jsonBody.data[k][key] + '" hidden';  
                                   }else{
                                        if (value.type == 'number'){
                                            var spojen = '<tr><th scope="col">' + key + '</th><td><input type="text" id="' + key +  '"value="' + jsonBody.data[k][key] + '"';
                                            spojen += ' onkeyup="this.value=this.value.replace(/[^\\d]/,\'\')" ';
                                        }else if(value.type == 'timestamp'){    
                                            var spojen = '<tr><th scope="col">' + key + '</th><td><input type="datetime-local" id="' + key +  '"value="' + jsonBody.data[k][key] + '"';     
                                        }else if(value.type == 'date'){
                                            var datum = jsonBody.data[k][key].substring(0, 10);     
                                            var spojen = '<tr><th scope="col">' + key + '</th><td><input type="date" id="' + key +  '"value="' + datum + '"';     
                                        }else{
                                            var spojen = '<tr><th scope="col">' + key + '</th><td><input type="text" id="' + key +  '"value="' + jsonBody.data[k][key] + '"';     
                                        }
                                   }
                                   
                                   
                               }else{
                                   var spojen = '<tr><th scope="col">' + key + '</th><td><input type="text" id="' + key +  '"value="' + jsonBody.data[k][key] + '"'; 
                               }
                               if (value.maxlength){
                                   spojen += ' maxlength="' + value.maxlength + '"' + '></td></tr>'; 
                                }else{
                                   spojen += '></td></tr>'; 
                                }   
                                tablica += spojen;
                           } 
                       }  
                    });
                    tablica += '</table>'; 
                });
                tablica += '<button type="button" class="btn btn-warning" onclick="saveRow(\'' + fakeJson + '\')">Spremi <i class="fas fa-save"></i></button> ';
                tablica += '<button type="button" class="btn btn-success" onclick="showKorisnici(' + inJson.page + ')">Odustani <i class="fas fa-window-close"></i></button>';
                $("#container").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

function insertRow(inJson){
    fakeJson = inJson;
    inJson = JSON.parse(inJson.replace(/~/g, "\""));
    var tablica = '<table class="table table-hover" id="promjena"><tbody>';

    $.each(inJson.columns, function (key, value) { 
        if (key != 'ID') {
            if (value.function){
                var funkcija = value.function  + '(' + null + ',\'' + key + '\'' + ',' + value.lov + ')';
                if (value.alias){
                    tablica += '<tr><th scope="col">' + value.alias + '</th><td>' + eval(funkcija) + '</td></tr>';
                }else{
                    tablica += '<tr><th scope="col">' + key + '</th><td>' + eval(funkcija) + '</td></tr>';
                }     
            }else{
                if(value.type){
                    if(value.hidden){
                        var spojen = '';                          
                    }else{
                        if (value.alias){
                            var spojen = '<tr><th scope="col">' + value.alias + '</th>';
                        }else{
                            var spojen = '<tr><th scope="col">' + key + '</th>';
                        }
                        if (value.type == 'number'){
                            spojen += '<td><input type="text" id="' + key +  '"value="' + '' + '"';
                            spojen += ' onkeyup="this.value=this.value.replace(/[^\\d]/,\'\')" ';
                        }else{
                            spojen += '<td><input type="text" id="' + key +  '"value="' + '' + '"';     
                        }
                    }
                    
                    
                }else{
                    var spojen = '<tr><th scope="col">' + key + '</th><td><input type="text" id="' + key +  '"value="' + '' + '"'; 
                }
                if(value.hidden){
                    spojen += ''; 
                }else{
                    if (value.maxlength){
                        spojen += ' maxlength="' + value.maxlength + '"' + '></td></tr>'; 
                     }else{
                        spojen += '></td></tr>'; 
                     }   
                }    
                
                 tablica += spojen;
            } 
        }  
     });
     tablica += '</table>'; 
     tablica += '<button type="button" class="btn btn-warning" onclick="saveRow(\'' + fakeJson + '\')">Spremi <i class="fas fa-save"></i></button> ';
     tablica += '<button type="button" class="btn btn-success" onclick="showKorisnici(' + inJson.page + ')">Odustani <i class="fas fa-window-close"></i></button>';
     $("#container").html(tablica);
}
//----------------------------------------------------------------------------------------------
function saveRow(inJson) {
    fakeJson = inJson;
    inJson = JSON.parse(inJson.replace(/~/g, "\""));
    var greska = 0;
    $.each(inJson.columns, function (k, v) {
        if(k != 'ID'){
            if(v.required){
                if (!$( "#" + k ).val()){
                    alert(v.message);
                    greska = 1;
                    return false;
                } 
            }
        } 
    });

    if (greska > 0){
        return;
    }
    
    var arrIDs = $.map($("#promjena [id]"), function(n, i) {
        return n.id;
    });

    inData = {"projekt": inJson.projekt, "procedura": inJson.procedura.save};

    $.each( arrIDs, function( i, val ) {
        var vrijednost = $( "#" + val ).val();
        if(!vrijednost){
            vrijednost = '';
        }
        inData[val] = vrijednost;
    });
    
    $.ajax({
        type: 'POST',
        url: url,
        data: inData,
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                Swal.fire('Uspješno se unijeli korisnika');
            } else {
                Swal.fire(message + '.' + errcode);
            }
            //refresh();
            //showZaposlenici();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true
    });
}
