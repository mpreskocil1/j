<?php
require_once('poruke.php');
require_once('db_credential.php');

function f_get_korisniki($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM korisniki where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->page*($in_obj->page-1);
       $sql = "SELECT * FROM korisniki WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from korisniki");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_korisniki($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
  
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE korisniki SET "
          . "IDpoduzeca = '$in_obj->IDpoduzeca', "
          . "ime = '$in_obj->ime', "
          . "prezime = '$in_obj->prezime', "
          . "OIB = '$in_obj->OIB', "
          . "email = '$in_obj->email', "
          . "password = '$in_obj->password', "
          . "spol = '$in_obj->spol', "
          . "slika =  $in_obj->slika , "
          . "ovlasti =  $in_obj->ovlasti , " 
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO korisniki (ID, IDpoduzeca, ime, prezime,OIB, email, password, spol, slika, ovlasti, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->IDpoduzeca ."'" . "," 
	      . "'".$in_obj->ime ."'" . "," 
	      . "'".$in_obj->prezime ."'" . "," 
          . "'".$in_obj->OIB  ."'". ","           
	      . "'". $in_obj->email  . "'" . ","
          . "'".$in_obj->password  ."'" .","
          .  $in_obj->spol  .","
          .  $in_obj->slika  . ","
          .  $in_obj->ovlasti  . ","
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_korisniki($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE korisniki SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------


function f_get_banke($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM banke where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM banke WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from banke");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_banke($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE banke SET "
          . "VBDI = '$in_obj->vbdi', "
          . "NAZIV = '$in_obj->naziv', "
          . "SWIFT = '$in_obj->swift', "
          . "ADRESA = '$in_obj->adresa', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO banke (ID, VBDI, NAZIV, SWIFT,ADRESA, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->VBDI ."'" . "," 
	      . "'".$in_obj->NAZIV ."'" . "," 
	      . "'".$in_obj->SWIFT ."'" . "," 
          . "'".$in_obj->ADRESA  ."'". ","           
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_banke($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE banke SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------

function f_get_doprinosi($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM doprinosi where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM doprinosi WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from doprinosi");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_doprinosi($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE doprinosi SET "
          . "IDKORISNIKA = '$in_obj->vbdi', "
          . "NAZIV = '$in_obj->naziv', "
          . "STOPADOP = '$in_obj->swift', "
          . "VRSTA = '$in_obj->adresa', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO doprinosi (ID, IDKORISNIKA, NAZIV, STOPADOP,VRSTA, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->IDKORISNIKA ."'" . "," 
	      . "'".$in_obj->NAZIV ."'" . "," 
	      . "'".$in_obj->STOPADOP ."'" . "," 
          . "'".$in_obj->VRSTA  ."'". ","           
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_doprinosi($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE doprinosi SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------------


function f_get_konta($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM konta where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM konta WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from konta");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_konta($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE konta SET "
          . "IDPODUZECA = '$in_obj->idpoduzeca', "
          . "SIFRA = '$in_obj->sifra', "
          . "NAZIV = '$in_obj->naziv', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO konta (ID, IDPODUZECA, SIFRA, NAZIV, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->IDPODUZECA ."'" . "," 
	      . "'".$in_obj->SIFRA ."'" . "," 
	      . "'".$in_obj->NAZIV ."'" . ","           
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_konta($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE konta SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//-----------------------------------------------------------------------------------------


function f_get_partneri($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM partneri where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM partneri WHERE DELETED = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from partneri");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_partneri($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE partneri SET "
          . "IDPODUZECA = '$in_obj->vbdi', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO partneri (ID, IDPODUZECA, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->IDPODUZECA ."'" . ","          
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_partneri($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE partneri SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//-----------------------------------------------------------------


function f_get_poduzeca($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM poduzeca where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM poduzeca WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from poduzeca");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_poduzeca($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE poduzeca SET "
          . "NAZIV = '$in_obj->naziv', "
          . "OIB = '$in_obj->oib', "
          . "ADRESA = '$in_obj->adresa', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO poduzeca (ID, NAZIV, OIB, ADRESA, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->NAZIV ."'" . "," 
	      . "'".$in_obj->OIB ."'" . "," 
	      . "'".$in_obj->ADRESA ."'" . ","          
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_poduzeca($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE poduzeca SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}


//---------------------------------------------------------------------

function f_get_porezi($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM porezi where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM porezi WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from porezi");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_porezi($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE porezi SET "
          . "NAZIV = '$in_obj->naziv', "
          . "PDV = '$in_obj->pdv', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO porezi (ID, NAZIV, PDV, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->NAZIV ."'" . "," 
	      . "'".$in_obj->PDV ."'" . ","          
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_porezi($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE porezi SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//--------------------------------------------------------------------

function f_get_porezneopcine($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM porezneopcine where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM porezneopcine WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from porezneopcine");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_porezneopcine($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE porezneopcine SET "
          . "NAZIV = '$in_obj->naziv', "
          . "POSTOTAKPRIREZA = '$in_obj->postotakprireza', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO porezneopcine (ID, NAZIV, POSTOTAKPRIREZA, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->NAZIV ."'" . "," 
	      . "'".$in_obj->POSTOTAKPRIREZA ."'" . ","            
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_porezneopcine($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE porezneopcine SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//----------------------------------------------------------

function f_get_poste($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM poste where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM poste WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from poste");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_poste($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE poste SET "
          . "NAZIV = '$in_obj->naziv', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO poste (ID,NAZIV, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . ","
          . "'".$in_obj->NAZIV ."'" . ","       
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_poste($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE poste SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//-----------------------------------------------------------

function f_get_skladista($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM skaldista where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM skladista WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from skladista");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_skladista($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE skladista SET "
          . "IDPODUZECA = '$in_obj->idpoduzeca', "
          . "NAZIV = '$in_obj->naziv', "
          . "MP_VP = '$in_obj->mp_vp', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO skladista (ID, IDPODUZECA,NAZIV, MP_VP, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . ","
          . "'".$in_obj->IDPODUZECA ."'" . ","  
          . "'".$in_obj->NAZIV ."'" . "," 
	      . "'".$in_obj->MP_VP ."'" . ","          
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_skladista($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE skladista SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//-------------------------------------------------------------------------

function f_get_temeljnica($db, $in_obj){ 
    if (isset($in_obj->ID)){
        $sql = "SELECT * FROM temeljnica where ID = " . $in_obj->ID;
    }else{
       $offset = $in_obj->perPage*($in_obj->page-1);
       $sql = "SELECT * FROM temeljnica WHERE Deleted = 0 LIMIT $in_obj->perPage OFFSET $offset";
       $output['count'] = f_get_count($db, "SELECT count(1) from temeljnica");
    }
    $output['data'] = f_get_rows($db, $sql); 
    echo json_encode($output);
}

function f_save_temeljnica($db, $in_obj){ 
    global $insert_error;
    global $insert_pass;
    global $update_error; 
    global $update_pass;
    
    if (isset($poruka)){
        echo $poruka;
        return;
    }
    
    if (isset($in_obj->ID)){
        $sql = "UPDATE temeljnica SET "
          . "NAZIV = '$in_obj->naziv', "
          . " WHERE ID =  $in_obj->ID";

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $update_pass;
        }else{
            echo $update_error;   
        } 
    }else{
        $sql = "INSERT INTO temeljnica (ID, NAZIV, deleted) VALUES "
          . "( " 
          . "'".$in_obj->ID ."'" . "," 
          . "'".$in_obj->NAZIV ."'" . ","           
          .  $in_obj->deleted  . ","
          .  0  
          .")"; 
          
          $db->set_charset("utf8");
          if ($db->query($sql) === TRUE) {
              echo $insert_pass;
          }else{
              echo $insert_error;   
          }  
    }  
}

function f_delete_temeljnica($db, $in_obj){ 
    global $delete_error;
    global $delete_pass;

    if (isset($in_obj->ID)){
        $sql = "UPDATE temeljnica SET "
        . "deleted = 1"
        . " WHERE ID = "  .$in_obj->ID;

        $db->set_charset("utf8");
        if ($db->query($sql) === TRUE) {
            echo $delete_pass;
        }else{
            echo $delete_error;   
        } 
    }else{
        echo $delete_error;
    }
}

//----------------------------------------------------------------

//funkcija za provjeru logina
function f_login($db, $in_obj){
    global $wrong_login;
    $sql = "SELECT * FROM korisniki WHERE EMAIL = '$in_obj->username' AND PASSWORD = '$in_obj->password'";
    $rows=[];
    $db->set_charset("utf8");    
    $result = $db->query($sql);
    while($row = mysqli_fetch_assoc($result)) {
        $rows[]=$row;
    }

    if (!empty($rows)){
        $_SESSION = $rows[0];
        echo json_encode($rows);
    }else{
        echo $wrong_login;
    }
}

//konekcija na bazu
function f_get_database(){
    $db = new mysqli(DB_SERVER,DB_USER,DB_PASS,DB_NAME);
    if($db->connect_errno){
    throw new Exception("Neuspješna konekcija na bazu");
    }
    return $db;
   }

 // dohvaćanje podataka
function f_get_rows($db, $sql){
    $db->set_charset("utf8");    
    $result = $db->query($sql);
    $rows=[];
    while($row = mysqli_fetch_assoc($result)) {
        $rows[]=$row;
    }    
    return $rows;
}

// dohvaćanje broja redova
function f_get_count($db, $sql){
    $result = $db->query($sql);
    $row = mysqli_fetch_assoc($result);
    return $row['count(1)'];
}

?>