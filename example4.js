var url = "router.php";
//mora ostati radi stare procedure
var perPage = 20;

$(".dropdown-content").click(function (event) {
    var parent_id = $(event.target).parent().parent().find("button:first-child").text();
    $("#breadcrumbs").html('<b>' + parent_id + ' > ' + $(event.target).text() + '</b>');
});

$(document).ready(function () {
    refresh();
    var korisniciTbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_users", 
                                     "save":"p_save_user"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDpoduzeca":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "maxlength":9,
                                         "message":"Molimo odaberite IDpoduzeca"},
                                   "ime":{"show":true,
                                          "required":true,
                                          "type":"string",
                                          "maxlength":60, 
                                          "message":"Molimo unesite ime korisnika",
                                          "alias":"Ime"},     
                                   "prezime":{"show":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":60, 
                                              "message":"Molimo unesite prezime korisnika",
                                              "alias":"Prezime"},
                                   "OIB":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":11, 
                                           "message":"Molimo unesite OIB korisika"},                       
                                   "email":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite e_mail korisnika",
                                           "alias":"E-mail"},
                                    "spol":{"show":true,
                                           "required":true, 
                                           "function":"getSelect",
                                           "lov":"lovSpol",
                                           "message":"Molimo odaberite spol korisnika",
                                           "alias":"Spol"},
                                    "ovlasti":{"show":true,
                                               "required":true, 
                                               "function":'getSelect',
                                               "lov":"lovOvlasti",
                                               "message":"Molimo odaberite ovlasti korisnika",
                                               "alias":"Ovlasti"}                                                                           
                        },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(korisniciTbl)                    
    $("#container").html(showTable(korisniciTbl));
});


//hendlanje link button-a
//-------------------------------------------------------------
$("#loginBtn").click(function () {
    $("#container").html(loginForm);
});

$("#logoutBtn").click(function () {
    logout();
});

$("#pregledBtn").click(function () {
    showKorisnici();
});

// PODUZEĆE STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#poduzece").click(function () {
    showPoduzeca();
});

function showPoduzeca(){
    var poduzeceTbl = {"projekt": "projektweb2", 
                        "procedura":{"get":"p_get_poduzeca", 
                                     "save":"p_save_poduzeca"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                   "NAZIV":{"show":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":60, 
                                              "message":"Molimo unesite naziv poduzeća"},     
                                   "OIB":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":11, 
                                           "message":"Molimo unesite OIB poduzeća"},                       
                                   "ADRESA":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":100, 
                                           "message":"Molimo unesite adresu poduzeća"}   
                        },               
                        "action": true,
                        "edit": true,
                        "delete": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(poduzeceTbl)                    
    $("#container").html(showTable(poduzeceTbl));

}

//BANKE STRANICA
//--------------------------------------------------------------------------------------------------------------------------------------
$("#banke").click(function () {
    showBanke();
});

function showBanke(){
    var bankeTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_banke", 
                                     "save":"p_save_banke"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "VBDI":{"show":true,
                                         "required":true, 
                                         "type":"number",
                                         "maxlength":7, 
                                         "message":"Molimo unesite VBDI banke"},
                                   "NAZIV":{"show":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":60, 
                                              "message":"Molimo unesite naziv banke"},
                                    "SWIFT":{"show":true,
                                              "required":true, 
                                              "type":"string",
                                              "maxlength":10, 
                                              "message":"Molimo unesite SWIFT banke"},
                                   "ADRESA":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":100, 
                                           "message":"Molimo unesite adresu banke"}   
                        },               
                        "action": true, 
                        "insert": true, 
                        "edit":true,
                        "delete":true,
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(bankeTbl)                    
    $("#container").html(showTable(bankeTbl));

}

// POSLOVNI PARTNERI STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#poslovni_partneri").click(function () {
    showPoslovniPartneri();
});

function showPoslovniPartneri(){
    var poslovniPartneriTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_partneri", 
                                     "save":"p_save_partneri"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "NAZIV":{"show":true,
                                              "hidden":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":60, 
                                              "message":"Molimo unesite partnere"},
                                    "OIB":{"show":true,
                                           "required":false, 
                                           "type":"number",
                                           "maxlength":11,
                                           "hidden":true, 
                                           "message":"Molimo unesite OIB"},
                                    "ADRESA":{"show":true,
                                           "required":false, 
                                           "type":"string",
                                           "hidden":true,
                                           "maxlength":100, 
                                           "message":"Molimo unesite adresu"}
                        },               
                        "action": true, 
                        "edit":true,
                        "delete": true,
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(poslovniPartneriTbl)                    
    $("#container").html(showTable(poslovniPartneriTbl));

}

// SKLADIŠTA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#skladista").click(function () {
    showSkladista();
});

function showSkladista(){
    var skladistaTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_skladista", 
                                     "save":"p_save_skladista"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                   "IDPODUZECA":{"show":false,
                                         "hidden":true,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},      
                                   "NAZIV":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv skladišta",
                                           "alias":"Naziv"},
                                    "MP_VP":{"show":true,
                                           "required":true, 
                                           "function":'getSelect',
                                           "lov":"lovMP_VP", 
                                           "message":"Molimo odaberite koja je prodaja"}   
                        },               
                        "action": true,
                        "edit":true, 
                        "delete":true,
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(skladistaTbl)                    
    $("#container").html(showTable(skladistaTbl));

}

// TEMELJNICE STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#temeljnice").click(function () {
    showTemeljnice();
});

function showTemeljnice(){
    var temeljniceTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_temeljnica", 
                                     "save":"p_save_temeljnice"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                   "NAZIV":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "alias":"NAZIV",
                                           "message":"Molimo unesite naziv temeljnice"}
                        },               
                        "action": true, 
                        "edit":true,
                        "delete":true,
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(temeljniceTbl)                    
    $("#container").html(showTable(temeljniceTbl));

}

// KONTNI PLAN STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#kontni_plan").click(function () {
    showKontniPlan();
});

function showKontniPlan(){
    var kontniPlanTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_konta", 
                                     "save":"p_save_konta"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDPODUZECA":{"show":false,
                                         "required":true, 
                                         "type":"int", 
                                         "maxlength":9, 
                                         "message":"Molimo unesite ID  poduzeca"},
                                    "SIFRA":{"show":true,
                                         "required":true, 
                                         "type":"string", 
                                         "maxlength":10, 
                                         "message":"Molimo unesite šifru konta"},
                                   "NAZIV":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv konta"}  
                        },               
                        "action": true, 
                        "edit":true,
                        "delete":true,
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(kontniPlanTbl)                    
    $("#container").html(showTable(kontniPlanTbl));

}

// POŠTE STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#poste").click(function () {
    showPoste();
});

function showPoste(){
    var posteTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_poste", 
                                     "save":"p_save_poste"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "NAZIV":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv pošte"}               
                        },               
                        "action": true, 
                        "edit":true,
                        "delete":true,
                        "insert": true, 
                        "page": 1, 
                        "perPage": 15,
                        "where":"#container"
                        };
    showTable(posteTbl)                    
    $("#container").html(showTable(posteTbl));

}

// POREZNE OPĆINE STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#porezne_opcine").click(function () {
    showPorezneOpcine();
});

function showPorezneOpcine(){
    var porezneOpcineTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_porezneopcine", 
                                     "save":"p_save_prirezi"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "NAZIV":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv porezne općine"}, 
                                    "POSTOTAKPRIREZA":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":3, 
                                           "message":"Molimo unesite postotak prireza"}, 
                        },               
                        "action": true, 
                        "edit":true,
                        "delete":true,
                        "insert": true, 
                        "page": 1, 
                        "perPage": 10,
                        "where":"#container"
                        };
    showTable(porezneOpcineTbl)                    
    $("#container").html(showTable(porezneOpcineTbl));

}

// POREZI STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#porezi").click(function () {
    showPorezi();
});

function showPorezi(){
    var poreziTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_porezi", 
                                     "save":"p_save_porezi"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "NAZIV":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv poreza"}, 
                                    "PDV":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":3, 
                                           "message":"Molimo unesite iznos poreza"}, 
                        },               
                        "action": true, 
                        "edit":true,
                        "delete":true,
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(poreziTbl)                    
    $("#container").html(showTable(poreziTbl));

}

// DOPRINOSI STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#doprinosi").click(function () {
    showDoprinosi();
});

function showDoprinosi(){
    var doprinosiTbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_doprinosi", 
                                     "save":"p_save_doprinosi"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDPODUZECA":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "maxlength":9, 
                                         "message":"Molimo unesite ID "}, 
                                    "NAZIV":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":255, 
                                           "message":"Molimo unesite naziv doprinosa"}, 
                                    "STOPADOP":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":3, 
                                           "message":"Molimo unesite stopu doprinosa"},
                                    "VRSTA":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":255, 
                                           "message":"Molimo unesite vrstu doprinosa"}
                        },               
                        "action": true, 
                        "insert": true, 
                        "edit":true,
                        "delete":true,
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(doprinosiTbl)                    
    $("#container").html(showTable(doprinosiTbl));

}

// KORISNICI1 STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#korisnici1").click(function () {
    showKorisnici1();
});

function showKorisnici1(){
   var korisnici1Tbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_korisnici", 
                                     "save":"p_save_user"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDPODUZECA":{"show":false,
                                         "required":true,
                                         "type":"number",
                                         "maxlength":9, 
                                         "message":"Molimo unesite ID korisnika"},
                                   "IME":{"show":true,
                                          "required":true,
                                          "type":"string",
                                          "maxlength":60, 
                                          "message":"Molimo unesite ime korisnika"},     
                                   "PREZIME":{"show":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":60, 
                                              "message":"Molimo unesite prezime korisnika"},
                                   "OIB":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":11, 
                                           "message":"Molimo unesite OIB korisika"},                       
                                   "EMAIL":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite e_mail korisnika"},
                                    "PASSWORD":{"show":true,
                                           "required":false, 
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo odaberite password korisnika"},
                                    "SPOL":{"show":true,
                                           "required":false, 
                                           "type":"number",
                                           "maxlength":1, 
                                           "message":"Molimo odaberite spol korisnika"},
                                    "SLIKA":{"show":true,
                                           "required":false, 
                                           "type":"string",
                                           "maxlength":255, 
                                           "message":"Molimo odaberite sliku korisnika"},
                                    "OVLASTI":{"show":true,
                                               "required":true, 
                                               "function":'getSelect',
                                               "lov":"lovOvlasti",
                                               "message":"Molimo odaberite ovlasti korisnika"}                                                                           
                        },               
                        "action": true, 
                        "insert": true, 
                        "edit":true,
                        "delete":true,
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(korisnici1Tbl)                    
    $("#container").html(showTable(korisnici1Tbl));

}

// RADNA MJESTA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#radna_mjesta").click(function () {
    showRadnaMjesta();
});

function showRadnaMjesta(){
   var radnaMjestaTbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_RADNA_MJESTA", 
                                     "save":"p_save_radna_mjesta"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "naziv":{"show":true,
                                          "required":true,
                                          "type":"string",
                                          "maxlength":60, 
                                          "message":"Molimo unesite naziv radnog mjesta",
                                          "alias":"Naziv"},     
                                   "opis":{"show":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":200, 
                                              "message":"Molimo unesite opis radnog mjesta",
                                              "alias":"Opis"},                       
                                   "kn_h":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite kune po satu",
                                           "alias":"Kuna po satu"},
                                  },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(radnaMjestaTbl)                    
    $("#container").html(showTable(radnaMjestaTbl));

}

// RADNICI STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#radnici").click(function () {
    showRadnici();
});

function showRadnici(){
   var radniciTbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_RADNICI", 
                                     "save":"p_save_radnici"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "radodnos":{"show":true,
                                          "required":true,
                                          "type":"string",
                                          "maxlength":60, 
                                          "message":"Molimo unesite radni odnos",
                                          "alias":"Radni odnos"},     
                                   "obraz":{"show":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":60, 
                                              "message":"Molimo unesite obrazovanje",
                                              "alias":"Obrazovanje"},                       
                                   "IDiban":{"show":false,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID ibana",},
                                    "IBAN":{"show":true,
                                           "required":false, 
                                           "type":"string", 
                                           "maxlength":21, 
                                           "message":"Molimo unesite IBAN"},
                                    "IDradmj":{"show":false,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID radnog mjesta"},
                                    "Radno mjesto":{"show":true,
                                           "required":false, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite radno mjesto"},
                                    "IDkor":{"show":false,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID korisnika"},
                                    "NAZIV":{"show":true,
                                           "required":false, 
                                           "type":"string", 
                                           "maxlength":90, 
                                           "message":"Molimo unesite naziv korisnika",
                                           "alias":"Naziv"}

                                  },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(radniciTbl)                    
    $("#container").html(showTable(radniciTbl));

}

// EVIDENCIJA O RADNOM VREMENU STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#evidencija_o_radnom_vremenu").click(function () {
    showEvidencijaORadnomVremenu();
});

function showEvidencijaORadnomVremenu(){
    var evidencijaORadnomVremenuTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_evidencija", 
                                      "save":"p_save_evidencija"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDradnik":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID radnika"}, 
                                    "Radnik":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite radnika"},    
                                    "mjesec":{"show":true,
                                               "required":false, 
                                               "type":"date",
                                              // "maxlength":60, 
                                               "message":"Molimo unesite mjesec",
                                               "alias":"Mjesec"},                       
                                    "datum":{"show":true,
                                            "required":true, 
                                            "type":"date", 
                                            //"maxlength":9, 
                                            "message":"Molimo unesite datum",
                                            "alias":"Datum"},
                                    "pocrada":{"show":true,
                                            "required":true, 
                                            "type":"date", 
                                            //"maxlength":9, 
                                            "message":"Molimo unesite početak rada",
                                            "alias":"Početak rada"},
                                    "zavrada":{"show":true,
                                            "required":true, 
                                            "type":"date", 
                                            //"maxlength":9, 
                                            "message":"Molimo unesite završetak rada",
                                            "alias":"Završetak rada"},                      
                                    "ukradvr":{"show":true,
                                           "required":true, 
                                           "type":"timestamp", 
                                           "maxlength":6, 
                                           "message":"Molimo unesite ukupno radno vrijeme",
                                           "alias":"Ukupno radno vrijeme"},
                                    "gododmor":{"show":true,
                                           "required":true, 
                                           "type":"char", 
                                           "maxlength":1, 
                                           "message":"Molimo unesite godišnji odmor",
                                           "alias":"Godišnji odmor"},
                                    "blagd_nerdan":{"show":true,
                                           "required":true, 
                                           "type":"char", 
                                           "maxlength":1, 
                                           "message":"Molimo unesite blagdane/ne radne dane",
                                           "alias":"Blagdani/Neradni dani"},
                                    "bolovanje":{"show":true,
                                           "required":true, 
                                           "type":"char", 
                                           "maxlength":1, 
                                           "message":"Molimo unesite bolovanje",
                                           "alias":"Bolovanje"},                       
                                    "putnal":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":1, 
                                           "message":"Molimo unesite putni nalog",
                                           "alias":"Putni nalozi"},
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(evidencijaORadnomVremenuTbl)                    
     $("#container").html(showTable(evidencijaORadnomVremenuTbl));
 
 }

// PUTNI NALOZI STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#putni_nalozi").click(function () {
    showPutniNalozi();
});

function showPutniNalozi(){
    var putniNaloziTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_KNJIGA_NALOGA", 
                                      "save":"p_save_nalozi"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDkupac":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID kupca"},
                                    "Poduzeće":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite poduzeće"},      
                                    "datum_narudzbe":{"show":true,
                                               "required":false, 
                                               "type":"date",
                                              // "maxlength":60, 
                                               "message":"Molimo unesite datum narudžbe",
                                               "alias":"Datum narudžbe"},                       
                                    "broj_nabave":{"show":true,
                                            "required":true, 
                                            "type":"number", 
                                            "maxlength":6, 
                                            "message":"Molimo unesite broj narudžbe",
                                            "alias":"Broj nabave"},
                                    "datum_dostave":{"show":true,
                                            "required":true, 
                                            "type":"date", 
                                            //"maxlength":9, 
                                            "message":"Molimo unesite datum dostave",
                                            "alias":"Datum dostave"},
                                    "broj_izlaz":{"show":true,
                                            "required":true, 
                                            "type":"date", 
                                            //"maxlength":9, 
                                            "message":"Molimo unesite datum izlaza robe",
                                            "alias":"Broj izlaza"},                      
                                    "dat_racuna":{"show":true,
                                           "required":true, 
                                           "type":"date", 
                                           //"maxlength":6, 
                                           "message":"Molimo unesite datum računa",
                                           "alias":"Datum računa"},
                                    "iznos":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":10, 
                                           "message":"Molimo unesite iznos",
                                           "alias":"Iznos"},
                                    "br_izvatka":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":6, 
                                           "message":"Molimo unesite broj izvatka",
                                           "alias":"Broj izvatka"},             
                                    "doznaceni_iznos":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":10, 
                                           "message":"Molimo unesite doznačeni iznos",
                                           "alias":"Doznačeni iznos"},
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(putniNaloziTbl)                    
     $("#container").html(showTable(putniNaloziTbl));
 
 }

// GLAVNA KNJIGA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#glavna_knjiga").click(function () {
    showGlavnaKnjiga();
});

function showGlavnaKnjiga(){
    var glavnaKnjigaTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_glavna_knjiga", 
                                      "save":"p_save_glavna_knjiga"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "ID_konta":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID konta"},     
                                    "IDpartneri":{"show":false,
                                               "required":false, 
                                               "type":"number",
                                               "maxlength":9, 
                                               "message":"Molimo unesite ID partnera"},
                                    "Temeljnica":{"show":true,
                                               "required":false, 
                                               "type":"string",
                                               "maxlength":60,
                                               "message":"Molimo unesite temeljnicu"},
                                    "Konto":{"show":true,
                                               "required":false,
                                               "type":"string",
                                               "maxlength":60, 
                                               "message":"Molimo unesite konta"},
                                    "Tvrtke":{"show":true,
                                               "required":false, 
                                               "type":"string",
                                               "maxlength":60, 
                                               "message":"Molimo unesite tvrtku"},                        
                                    "nazkonta":{"show":true,
                                            "required":true, 
                                            "type":"string", 
                                            "maxlength":60, 
                                            "message":"Molimo unesite naziv konta",
                                            "alias":"Naziv konta"},
                                    "opisknjiz":{"show":true,
                                            "required":true, 
                                            "type":"string", 
                                            //"maxlength":60, 
                                            "message":"Molimo unesite opis knjiženja",
                                            "alias":"Opis knjiženja"},
                                    "duguje":{"show":true,
                                            "required":true, 
                                            "type":"float", 
                                            "maxlength":60, 
                                            "message":"Molimo unesite iznos duguje",
                                            "alias":"Duguje"},                      
                                    "potrazuje":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite iznos potrazuje",
                                           "alias":"Potražuje"},
                                    "ukupnodug":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite iznos ukupnog duga",
                                           "alias":"Ukupno duguje"},
                                    "ukupnopot":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite iznos ukupne potražnje",
                                           "alias":"Ukupno potražuje"},             
                                    "datum":{"show":true,
                                           "required":true, 
                                           "type":"date",
                                           "message":"Molimo unesite datum",
                                           "alias":"Datum"},
                                    "IDtemelj":{"show":false,
                                           "required":true, 
                                           "type":"number",
                                           "maxlength":9,
                                           "message":"Molimo unesite ID temeljnice"}       
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(glavnaKnjigaTbl)                    
     $("#container").html(showTable(glavnaKnjigaTbl));
 
 }

// PLAĆA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#placa").click(function () {
    showPlaca();
});

function showPlaca(){
    var placaTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_KALKPLACA", 
                                      "save":"p_save_kalkplaca"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "bruto":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite bruto iznos",
                                           "alias":"Bruto"},     
                                    "mio1":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite mio1",
                                           "alias":"MIO1"},
                                    "mio2":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite mio2",
                                           "alias":"MIO2"},     
                                    "dohodak":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite dohodak",
                                           "alias":"Dohodak"},
                                    "odbitak":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite odbitak",
                                           "alias":"Odbitak"},     
                                    "osnzaporez":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite osnovu za porez",
                                           "alias":"Osnovni porez"},
                                    "pordo17500":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite porez manji od 17500,00 kn",
                                           "alias":"Porez do 17.500,00 kn"},     
                                    "porod17500":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite porez veći od 17500,00 kn",
                                           "alias":"Porez od 17.500,00 kn"}, 
                                    "ukporez":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite ukupni porez",
                                           "alias":"Ukupan porez"},     
                                    "prirez":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite prirez",
                                           "alias":"Prirez"},
                                    "ukporipri":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite ukupan porez i prirez",
                                           "alias":"Ukupni prirez"},     
                                    "neto":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite neto iznos",
                                           "alias":"Neto"},
                                    "zdrav":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite iznos zdravstvenog osiguranja",
                                           "alias":"Zdravstveno osiguranje"},     
                                    "zaposljavanje":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite iznos zapošljavanja",
                                           "alias":"Zapošljavanje"},
                                    "ozljede":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite iznos za ozljede",
                                           "alias":"Ozljede"},     
                                    "uktroskovi":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite ukupne troškove",
                                           "alias":"Ukupni troškovi"}                           
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(placaTbl)                    
     $("#container").html(showTable(placaTbl));
 
 }

// IZVJEŠĆA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#izvjesca").click(function () {
    showIzvjesca();
});

function showIzvjesca(){
    var izvjescaTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_IZVJESCA", 
                                      "save":"p_save_izvjesca"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDdnevknj":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID dnevnog knjiženja"},     
                                    "IDbrutbil":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID bruto bilance"},
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(izvjescaTbl)                    
     $("#container").html(showTable(izvjescaTbl));
 
 }

// KNJIGA ULAZA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#knjiga_ulaza").click(function () {
    showKnjigaUlaza();
});

function showKnjigaUlaza(){
    var knjigaUlazaTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_KNJIGA_ULAZ", 
                                      "save":"p_save_knjiga_ulaz"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDdobavljac":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID dobavljača"}, 
                                    "Dobavljač":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite dobavljača"},    
                                    "br_rac":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":6, 
                                           "message":"Molimo unesite broj računa",
                                           "alias":"Broj računa"},
                                    "datum_rac":{"show":true,
                                           "required":true,
                                           "type":"date",
                                           "message":"Molimo unesite datum",
                                           "alias":"Datum računa"},
                                    "oib":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":11, 
                                           "message":"Molimo unesite OIB"},
                                    "OIB":{"show":true,
                                           "required":false,
                                           "type":"number",
                                           "maxlength":11, 
                                           "message":"Molimo unesite OIB"},
                                    "iznos_bezpor":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":100, 
                                           "message":"Molimo unesite iznos bez poreza",
                                           "alias":"Iznos bez poreza"},
                                    "iznos_s_por":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":100, 
                                           "message":"Molimo unesite iznos s porezom",
                                           "alias":"Iznos sa porezom"},
                                    "PDV":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":100, 
                                           "message":"Molimo unesite Iznos s PDVom"}
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(knjigaUlazaTbl)                    
     $("#container").html(showTable(knjigaUlazaTbl));
 
 }

 // KNJIGA IZLAZA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#knjiga_izlaza").click(function () {
    showKnjigaIzlaza();
});

function showKnjigaIzlaza(){
    var knjigaIzlazaTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_KNJIGA_IZLAZ", 
                                      "save":"p_save_knjiga_izlaz"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDkupac":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID dobavljača"}, 
                                    "Kupac":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite dobavljača"},    
                                    "br_rac":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":6, 
                                           "message":"Molimo unesite broj računa",
                                           "alias":"Broj računa"},
                                    "datum_rac":{"show":true,
                                           "required":true,
                                           "type":"date",
                                           "message":"Molimo unesite datum",
                                           "alias":"Datum računa"},
                                    "oib":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":11, 
                                           "message":"Molimo unesite OIB"},
                                    "OIB":{"show":true,
                                           "required":false,
                                           "type":"number",
                                           "maxlength":11, 
                                           "message":"Molimo unesite OIB"},
                                    "stopa_poreza":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":100, 
                                           "message":"Molimo unesite iznos bez poreza",
                                           "alias":"Stopa poreza"},
                                    "iznos_s_por":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":100, 
                                           "message":"Molimo unesite iznos s porezom",
                                           "alias":"Iznos s porezom"},
                                    "osnovica":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":100, 
                                           "message":"Molimo unesite Iznos s PDVom",
                                           "alias":"Osnovica"},
                                    "porez":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":100, 
                                           "message":"Molimo unesite Iznos s PDVom",
                                           "alias":"Porez"}
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(knjigaIzlazaTbl)                    
     $("#container").html(showTable(knjigaIzlazaTbl));
 
 }

// ARTIKLI STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#artikli").click(function () {
    showArtikli();
});

function showArtikli(){
    var artikliTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_artikli", 
                                      "save":"p_save_artikli"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDsklad":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID skladišta"},
                                    "Skladište":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite skladište"},     
                                    "nazart":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv atrikla",
                                           "alias":"Naziv artikla"},
                                    "jedmj":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":5, 
                                           "message":"Molimo unesite jedinicu mjere",
                                           "alias":"Jedinica mjere"},     
                                    "opisart":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite opis artikla",
                                           "alias":"Opis artikla"},
                                    "cijenaart":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite cijenu artikla",
                                           "alias":"Cijena artikla"},     
                                    "dobavlj":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":40, 
                                           "message":"Molimo unesite ime dobavljača",
                                           "alias":"Dobavljač"}                           
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(artikliTbl)                    
     $("#container").html(showTable(artikliTbl));
 
 }

// SKLADIŠNE KARTICE STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#skladisne_kartice").click(function () {
    showSkladisneKartice();
});

function showSkladisneKartice(){
    var skladisneKarticeTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_SKLADISNE_KARTICE", 
                                      "save":"p_save_skladisne_kartice"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDpoduzeca":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9,
                                           "message":"Molimo unesite ID poduzeća"},     
                                    "IDskladista":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID skladišta"},
                                    "IDartikl":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID artikla"},
                                    "Poduzeće":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite poduzeća"},
                                    "Skladište":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite Skladište"},
                                    "Artikl":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite Artikl"},     
                                    "datum":{"show":true,
                                           "required":true,
                                           "type":"date", 
                                           "message":"Molimo unesite datum",
                                           "alias":"Datum"},
                                    "sadrzaj":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite sadržaj skladišta",
                                           "alias":"Sadržaj"},     
                                    "ulaz":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":5, 
                                           "message":"Molimo unesite ulaz",
                                           "alias":"Ulaz"},
                                    "izlaz":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":5, 
                                           "message":"Molimo unesite izlaz",
                                           "alias":"Izlaz"},     
                                    "stanje":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":5, 
                                           "message":"Molimo unesite stanje",
                                           "alias":"Stanje"},
                                    "cijena":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite cijenu",
                                           "alias":"Cijena"},     
                                    "minimum":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":6,  
                                           "message":"Molimo unesite minimum",
                                           "alias":"Minimum"},
                                    "maximum":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":6, 
                                           "message":"Molimo unesite maximum",
                                           "alias":"Maximum"}                               
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(skladisneKarticeTbl)                    
     $("#container").html(showTable(skladisneKarticeTbl));
 
 } 

// PRIMKA/KALKULACIJA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#primka_kalkulacija").click(function () {
    showPrimkaKalkulacija();
});

function showPrimkaKalkulacija(){
    var primkaKalkulacijaTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_KALKULACIJA", 
                                      "save":"p_save_kalkulacija"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDpoduzeca":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID poduzeća"},   
                                    "Poduzeće":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv poduzeća"},     
                                    "IDdobavljac":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID dobavljača"},
                                    "Dobavljač":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv dobavljača"},   
                                    "datum":{"show":true,
                                           "required":true,
                                           "type":"date", 
                                           "message":"Molimo unesite datum",
                                           "alias":"Datum"},
                                    "redbr":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":4, 
                                           "message":"Molimo unesite redni broj kalkulacije",
                                           "alias":"Redni broj"},     
                                    "sifart":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":8, 
                                           "message":"Molimo unesite šifru artikla",
                                           "alias":"Šifra artikla"},
                                    "Artikl":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv artikla"},
                                    "kol":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":5, 
                                           "message":"Molimo unesite količinu",
                                           "alias":"Količina"},     
                                    "nabcij":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite nabavnu cijenu",
                                           "alias":"Nabavna cijena"},
                                    "nabvrj":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite nabavnu vrijednost",
                                           "alias":"Nabavna vrijednost"},     
                                    "RUC":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":2,  
                                           "message":"Molimo unesite RUC"},
                                    "RUCiznos":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":4, 
                                           "message":"Molimo unesite iznos RUC-a",
                                           "alias":"Iznos RUC-a"},
                                    "RUCukupno":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":4, 
                                           "message":"Molimo unesite ukupan RUC",
                                           "alias":"RUC ukupno"},     
                                    "prodcj":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite prodajnu cijenu",
                                           "alias":"Prodajna cijena"},
                                    "PDV":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":2, 
                                           "message":"Molimo unesite vrijednost PDV-a"},     
                                    "PDViznos":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12,  
                                           "message":"Molimo unesite iznos PDV-a",
                                           "alias":"Iznos s PDV-om"},
                                    "prodcjPDV":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite prodajnu cijenu s PDV-om",
                                           "alias":"Prodajna cijena s PDV-om"}                                      
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(primkaKalkulacijaTbl)                    
     $("#container").html(showTable(primkaKalkulacijaTbl));
 
 }

// NARUDŽBENICA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#narudzbenica").click(function () {
    showNarudzbenica();
});

function showNarudzbenica(){
    var narudzbenicaTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_NARUDZBENICA", 
                                      "save":"p_save_narudzbenica"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDpoduzeca":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID poduzeća"},
                                    "Poduzeće":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite poduzeća"},
                                    "IDdob":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID dobavljača"},
                                    "Dobavljač":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite dobavljača"},
                                    "placanje":{"show":true,
                                           "required":true,
                                           "type":"string", 
                                           "maxlength":40,
                                           "message":"Molimo unesite opis plaćanja",
                                           "alias":"Plaćanje"},
                                    "rokispor":{"show":true,
                                           "required":true,
                                           "type":"date",
                                           "message":"Molimo unesite rok isporuke",
                                           "alias":"Rok isporuke"},     
                                    "dostava":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite naćin dostave",
                                           "alias":"Dostava"},
                                    "redbr":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":4, 
                                           "message":"Molimo unesite redni broj",
                                           "alias":"Redni broj"},     
                                    "IDartikla":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID artikla"},
                                    "Artikl":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite artikl"},
                                    "jedmj":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":4, 
                                           "message":"Molimo unesite jedinicu mjere",
                                           "alias":"Jedinica mjere"},     
                                    "kol":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":4,  
                                           "message":"Molimo unesite količinu",
                                           "alias":"Količina"},
                                    "cijena":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite iznos robe",
                                           "alias":"Cijena"},
                                    "iznos":{"show":true,
                                           "required":true,
                                           "type":"float",
                                           "maxlength":12, 
                                           "message":"Molimo unesite iznos",
                                           "alias":"Iznos"}                                      
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(narudzbenicaTbl)                    
     $("#container").html(showTable(narudzbenicaTbl));
 
 }

// OTPREMNICA STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#otpremnica").click(function () {
    showOtpremnica();
});

function showOtpremnica(){
    var otpremnicaTbl = {"projekt": "p_tvrtka", 
                         "procedura":{"get":"p_get_OTPREMNICA", 
                                      "save":"p_save_otpremnica"}, 
                         "input": false, 
                         "columns":{"ID":{"show":false,
                                          "required":true, 
                                          "type":"number", 
                                          "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDpoduzeca":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID poduzeća"},
                                    "Poduzeće":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite poduzeća"},
                                    "IDpartner":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID partnera"}, 
                                    "Partner":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite partnera"},     
                                    "datum":{"show":true,
                                           "required":true,
                                           "type":"date",
                                           "message":"Molimo unesite datum",
                                           "alias":"Datum"},
                                    "datumotp":{"show":true,
                                           "required":true,
                                           "type":"date",
                                           "message":"Molimo unesite datum otpremanja",
                                           "alias":"Datum otpreme"},
                                    "dostava":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":60,
                                           "message":"Molimo unesite naćin dostave",
                                           "alias":"Dostava"},     
                                    "franco":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite franco",
                                           "alias":"Franco"},
                                    "redbr":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":6, 
                                           "message":"Molimo unesite redni broj",
                                           "alias":"Redni broj"},     
                                    "IDartikla":{"show":false,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":9, 
                                           "message":"Molimo unesite ID artikla"},
                                    "Artikl":{"show":true,
                                           "required":false,
                                           "type":"string",
                                           "maxlength":60, 
                                           "message":"Molimo unesite artikl"},
                                    "jedmj":{"show":true,
                                           "required":true,
                                           "type":"string",
                                           "maxlength":4, 
                                           "message":"Molimo unesite jedinicu mjere",
                                           "alias":"Jednica mjere"},     
                                    "kol":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":4,  
                                           "message":"Molimo unesite količinu",
                                           "alias":"Količina"},
                                    "jedcijena":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":4, 
                                           "message":"Molimo unesite cijenu",
                                           "alias":"Cijena"},
                                    "iznosbezporez":{"show":true,
                                           "required":true,
                                           "type":"number",
                                           "maxlength":6, 
                                           "message":"Molimo unesite iznos bez poreza",
                                           "alias":"Iznos bez poreza"}                                      
                                   },               
                         "action": true, 
                         "insert": true, 
                         "page": 1, 
                         "perPage": 20,
                         "where":"#container"
                         };
     showTable(otpremnicaTbl)                    
     $("#container").html(showTable(otpremnicaTbl));
 }

// KONTNI PLAN1 STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#kontni_plan1").click(function () {
    showKontniPlan1();
});

function showKontniPlan1(){
    var kontniPlan1Tbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_KONTA", 
                                     "save":"p_save_konta"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDpoduzeca":{"show":false,
                                         "required":false, 
                                         "type":"number",
                                         "maxlength":9, 
                                         "message":"Molimo unesite ID poduzeća"},
                                    "sifra":{"show":true,
                                         "required":true, 
                                         "type":"number", 
                                         "maxlength":4, 
                                         "message":"Molimo unesite šifru konta",
                                         "alias":"Šifra"},
                                   "naziv":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv konta",
                                           "alias":"Naziv"}  
                        },               
                        "action": true,
                        "edit":true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(kontniPlan1Tbl)                    
    $("#container").html(showTable(kontniPlan1Tbl));

}

// POŠTE1 STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#poste1").click(function () {
    showPoste1();
});

function showPoste1(){
    var poste1Tbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_poste", 
                                     "save":"p_save_poste"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "naziv":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv pošte",
                                           "alias":"Naziv"}  
                        },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"

                        
                        };
    showTable(poste1Tbl)                    
    $("#container").html(showTable(poste1Tbl));

}

// POREZNE OPĆINE1 STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#porezne_opcine1").click(function () {
    showPorezneOpcine1();
});

function showPorezneOpcine1(){
    var porezneOpcine1Tbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_porezne_opcine", 
                                     "save":"p_save_porezne_opcine"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "naziv":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv porezne općine",
                                           "alias":"Naziv"}, 
                                    "postotakprireza":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":3, 
                                           "message":"Molimo unesite postotak prireza",
                                           "alias":"Postotak prireza"}, 
                        },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(porezneOpcine1Tbl)                    
    $("#container").html(showTable(porezneOpcine1Tbl));

}

// POREZI1 STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#porezi1").click(function () {
    showPorezi1();
});

function showPorezi1(){
    var porezi1Tbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_porezi", 
                                     "save":"p_save_porezi"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "naziv":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite naziv poreza",
                                           "alias":"Naziv"}, 
                                    "pdv":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":3, 
                                           "message":"Molimo unesite PDV",
                                           "alias":"PDV"}, 
                        },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(porezi1Tbl)                    
    $("#container").html(showTable(porezi1Tbl));

}

// DOPRINOSI1 STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#doprinosi1").click(function () {
    showDoprinosi1();
});

function showDoprinosi1(){
    var doprinosi1Tbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_doprinosi", 
                                     "save":"p_save_doprinosi"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDradnika":{"show":false,
                                         "required":false, 
                                         "type":"number",
                                         "maxlength":9, 
                                         "message":"Molimo unesite ID radnika"},
                                    "Radnik":{"show":true,
                                         "required":false, 
                                         "type":"number",
                                         "maxlength":9, 
                                         "message":"Molimo unesite radnika"},
                                    "naziv":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":255, 
                                           "message":"Molimo unesite naziv doprinosa",
                                           "alias":"Naziv"}, 
                                    "stopadop":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":3, 
                                           "message":"Molimo unesite stopu doprinosa",
                                           "alias":"Stopa doprinosa"},
                                    "vrsta":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":255, 
                                           "message":"Molimo unesite vrstu doprinosa",
                                           "alias":"Vrsta"}
                        },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(doprinosi1Tbl)                    
    $("#container").html(showTable(doprinosi1Tbl));

}

//BANKE1 STRANICA
//--------------------------------------------------------------------------------------------------------------------------------------
$("#banke1").click(function () {
    showBanke1();
});

function showBanke1(){
    var banke1Tbl = {"projekt": "p_ekonomska", 
                        "procedura":{"get":"p_get_banke", 
                                     "save":"p_save_banke"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},    
                                   "NAZIV":{"show":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":60, 
                                              "message":"Molimo unesite naziv banke"},
                                   "ADRESA":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":100, 
                                           "message":"Molimo unesite adresu banke"}   
                        },               
                        "action": true, 
                        "insert": true, 
                        "edit":true,
                        "delete":true,
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(banke1Tbl)                    
    $("#container").html(showTable(banke1Tbl));

}

// KORISNICI2 STRANICA
//------------------------------------------------------------------------------------------------------------------------------------
$("#korisnici2").click(function () {
    showKorisnici2();
});

function showKorisnici2(){
   var korisnici2Tbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_users", 
                                     "save":"p_save_user"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite korisnika kojeg želite mijenjati"},
                                    "IDpoduzeca":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "maxlength":9,
                                         "message":"Molimo odaberite IDpoduzeca"},
                                   "ime":{"show":true,
                                          "required":true,
                                          "type":"string",
                                          "maxlength":60, 
                                          "message":"Molimo unesite ime korisnika",
                                          "alias":"Ime"},     
                                   "prezime":{"show":true,
                                              "required":false, 
                                              "type":"string",
                                              "maxlength":60, 
                                              "message":"Molimo unesite prezime korisnika",
                                              "alias":"Prezime"},
                                   "OIB":{"show":true,
                                           "required":true, 
                                           "type":"number", 
                                           "maxlength":11, 
                                           "message":"Molimo unesite OIB korisika"},                       
                                   "email":{"show":true,
                                           "required":true, 
                                           "type":"string", 
                                           "maxlength":60, 
                                           "message":"Molimo unesite e_mail korisnika",
                                           "alias":"E-mail"},
                                    "spol":{"show":true,
                                           "required":true, 
                                           "function":"getSelect",
                                           "lov":"lovSpol",
                                           "message":"Molimo odaberite spol korisnika",
                                           "alias":"Spol"},
                                    "ovlasti":{"show":true,
                                               "required":true, 
                                               "function":'getSelect',
                                               "lov":"lovOvlasti",
                                               "message":"Molimo odaberite ovlasti korisnika",
                                               "alias":"Ovlasti"}                                                                           
                        },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(korisnici2Tbl)                    
    $("#container").html(showTable(korisnici2Tbl));

}

//BRUTO BILANCA STRANICA
//-----------------------------------------------------------------------------------------------------------------

$("#bruto_bilanca").click(function () {
    showBrutoBilanca();
});

function showBrutoBilanca(){
   var brutoBilancaTbl = {"projekt": "p_tvrtka", 
                        "procedura":{"get":"p_get_BRUTO_BILANCA", 
                                     "save":"p_save_bruto_bilanca"}, 
                        "input": false, 
                        "columns":{"ID":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "message":"Molimo odaberite bruto bilancu kojeg želite mijenjati"},
                                    "IDkonta":{"show":false,
                                         "required":true, 
                                         "type":"number", 
                                         "maxlength":9,
                                         "message":"Molimo odaberite ID konta"},
                                   "razdoblje_od":{"show":true,
                                          "required":true,
                                          "type":"date",
                                          "message":"Molimo unesite razdoblje od",
                                          "alias":"Razdoblje od"},     
                                   "razdoblje_do":{"show":true,
                                              "required":true, 
                                              "type":"date", 
                                              "message":"Molimo unesite razdoblje do",
                                              "alias":"Razdoblje do"},
                                   "bruto_dug":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":12, 
                                           "message":"Molimo unesite bruto dugovno", 
                                           "alias":"Bruto duguje"},                      
                                   "bruto_pot":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":12, 
                                           "message":"Molimo unesite bruto potražno",
                                           "alias":"Bruto potražno"},
                                    "saldo_dug":{"show":true,
                                           "required":true, 
                                           "type":"float", 
                                           "maxlength":12, 
                                           "message":"Molimo odaberite saldo duguje",
                                           "alias":"Saldo duguje"},
                                    "saldo_pot":{"show":true,
                                               "required":true, 
                                               "type":"float", 
                                               "maxlength":12, 
                                               "message":"Molimo odaberite saldo potražno",
                                               "alias":"Saldo potražuje"}                                                                           
                        },               
                        "action": true, 
                        "insert": true, 
                        "page": 1, 
                        "perPage": 20,
                        "where":"#container"
                        };
    showTable(brutoBilancaTbl)                    
    $("#container").html(showTable(brutoBilancaTbl));

}


/*function selectForm(procedura, ID, placeholder){
    var output = "<select id='"+ placeholder + "' style='width: 250px;'>";
    output += "<option disabled selected>Odaberite vrijednost</option>";
    $.ajax({
        type: 'POST',
        url: gURL,
        data: {"projekt": "p_tvrtka", "procedura": procedura},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcod = jsonBody.h_errcod;
            var message = jsonBody.h_message;
            if (message == null || message == "", errcod == null || errcod == 0) {
                $.each(jsonBody.data, function (k, v) {
                if (ID == v.ID){
                    output += "<option value='"+ v.ID +"' selected>"+ v.NAZIV +"</option>";   
                }else{
                    output += "<option value='"+ v.ID +"'>"+ v.NAZIV +"</option>"; 
                }  
            }) 
            }
            output += "</select>";  
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: false
    });
    */

function showKorisnici(page) {
    var tablica = '<br><button type="button" style="float:right;" class="btn btn-success" onclick="insertForm(' + page + ')">Insert <i class="fa fa-download" aria-hidden="true"></i></button><br><br>';
    tablica += '<table class="table table-hover"><tbody><thead><tr><th scope="col">ime</th><th scope="col">prezime</th><th scope="col">email</th><th scope="col">OIB</th><th scope="col">action</th></tr>';


    if (page == null || page == "") {
        page = 1;
    }

    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_tvrtka", "procedura": "p_user_insert", "perPage": perPage, "page": page },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            var count = jsonBody.count;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    tablica += '<tr><td>' + v.ime + '</td>';
                    tablica += '<td>' + v.prezime + '</td>';
                    tablica += '<td>' + v.email + '</td>';
                    tablica += '<td>' + v.OIB + '</td>';
                    tablica += '<td><button type="button" class="btn btn-primary" onclick="showKorisnik(' + v.ID + ',' + page + ')">Edit <i class="fas fa-edit"></i></button> ';
                    tablica += '<button type="button" class="btn btn-danger" onclick="delKorisnik(' + v.ID + ',' + page + ')">Delete <i class="far fa-trash-alt"></i></button></td></tr>';
                });
                tablica += '</tbody></table>';
                tablica += pagination(page, perPage, count);
                $("#container").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}




//kontrole za unos podataka
//------------------------LOGIN--------------------------------
$(document).on('click', '#getLogin', function () {
    var email = $('#inputEmail').val();
    var password = $('#inputPassword').val();
    if (email == null || email == "") {
        Swal.fire('Molimo unesite email adresu');
    } else if (password == null || password == "") {
        Swal.fire('Molimo unesite zaporku');
    } else {
        login();
    }
})

//-----------------------SAVE KORISNIK---------------------------
$(document).on('click', '#spremiKor', function () {
    var ime = $('#ime').val();
    var prezime = $('#prezime').val();
    var email = $('#email').val();
    var OIB = $('#OIB').val();
    var OVLASTI = $('#OVLASTI').val();
    var GODISTE = $('#GODISTE').val();
    var SPOL = $('#SPOL').val();
    var ID = $('#ID').val();
    console.log("ID=" + ID);

    if (ime == null || ime == "") {
        Swal.fire('Molimo unesite ime korisnika');
    } else if (prezime == null || prezime == "") {
        Swal.fire('Molimo unesite prezime korisnika');
    } else if (email == null || email == "") {
        Swal.fire('Molimo unesite email korisnika');
    } else if (OIB == null || OIB == "") {
        Swal.fire('Molimo unesite OIB korisnika');
    } else if (OVLASTI == null || OVLASTI == "") {
        Swal.fire('Molimo unesite ovlasti korisnika');
    } else if (SPOL == null || SPOL == "") {
        Swal.fire('Molimo unesite spol korisnika');
    } else if (GODISTE == null || GODISTE == "") {
        Swal.fire('Molimo unesite godište korisnika');
    } else {
        $.ajax({
            type: 'POST',
            url: url,
            data: { "projekt": "p_tvrtka", "procedura": "p_user_insert", "ID": ID, "ime": ime, "prezime": prezime, "email": email, "OIB": OIB, "OVLASTI": OVLASTI, "SPOL": SPOL, "GODISTE": GODISTE },
            success: function (data) {
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcode;
                var message = jsonBody.h_message;
                console.log(message + errcode);

                if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                    Swal.fire('Uspješno se unijeli korisnika');
                } else {
                    Swal.fire(message + '.' + errcode);
                }
                refresh();
                showKorisnici();
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: true
        });
    }
})
//-------------------------------------------------------------


$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});

function login() {
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_ekonomska", "procedura": "p_login", "username": $('#inputEmail').val(), "password": $('#inputPassword').val() },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcod = jsonBody.h_errcod;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcod == null || errcod == 0) {
                $("#container").html('');
            } else {
                Swal.fire(message + '.' + errcod);
            }
            $("#container").html('<h1>Vježbenička tvrtka, Ekonomska i birotehnička škola Bjelovar</h1><br>' + '<img src="skola.jpg" width="1075" height="666">' + '<img style="vertical-align:left" src="images.png" width="100" height="100"> <span style="vertical-align:left">Aplikacija za Ekonomsku i birotehničku školu Bjelovar izrađena od strane studenata Veleučilišta u Bjelovaru</span>');
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}


function refresh() {
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_common", "procedura": "p_refresh" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = '<small>ID:' + jsonBody.ID + '<br>' + 'ime prezime:' + jsonBody.IME + ' ' + jsonBody.PREZIME + '<br>' + 'email:' + jsonBody.EMAIL + '</small>';
            $("#podaci").html(podaci);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

function logout() {
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_tvrtka", "procedura": "p_logout" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "" || errcode == null) {
                Swal.fire("Greška u obradi podataka, molimo pokušajte ponovno!");
            } else {
                Swal.fire(message + '.' + errcode);
            }
            refresh();
            $("#container").html('');
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true
    });
}



function showKorisnik(ID, page) {
    var tablica = '<table class="table table-hover"><tbody>';
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_tvrtka", "procedura": "p_user_insert", "ID": ID },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    tablica += '<tr><th scope="col">ID</th><td><input type="text" id="ID" value="' + v.ID + '" readonly></td></tr>';
                    tablica += '<tr><th scope="col">ime</th><td><input type="text" id="ime" value="' + v.ime + '"></td></tr>';
                    tablica += '<tr><th scope="col">prezime</th><td><input type="text" id="prezime" value="' + v.prezime + '"></td></tr>';
                    tablica += '<tr><th scope="col">email</th><td><input type="text" id="email" value="' + v.email + '"></td></tr>';
                    tablica += '<tr><th scope="col">OIB</th><td><input type="text" id="OIB" value="' + v.OIB + '"></td></tr>';
                    tablica += '<tr><th scope="col">Ovlasti</th><td><input type="text" id="OVLASTI" value="' + v.OVLASTI + '"></td></tr>';
                    tablica += '<tr><th scope="col">Spol</th><td><input type="text" id="SPOL" value="' + v.SPOL + '"></td></tr>';
                    tablica += '<tr><th scope="col">godiste</th><td><input type="text" id="GODISTE" value="' + v.GODISTE + '"></td></tr></table>';
                    tablica += '<button type="button" class="btn btn-warning" id="spremiKor">Spremi <i class="fas fa-save"></i></button> ';
                    tablica += '<button type="button" class="btn btn-success" onclick="showKorisnici(' + page + ')">Odustani <i class="fas fa-window-close"></i></button>';
                });
                $("#container").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

function loginForm() {
    var output = "<br><br><form><div class='form-group'>";
    output += "<label for='inputEmail'>Email adresa</label>";
    output += "<input type='email' class='form-control' id='inputEmail' aria-describedby='emailHelp' placeholder='Enter email'></div>";
    output += "<div class='form-group'><label for='inputPassword'>Password</label>";
    output += "<input type='password' class='form-control' id='inputPassword' placeholder='Password'></div>";
    output += "<button class='btn btn-primary' id='getLogin' >Submit</button></form>";
    return output;
}


function insertForm(page) {
    var output = '<table class="table table-hover"><tbody>';
    output += '<tr><th scope="col">ime</th><td><input type="text" id="ime"></td></tr>';
    output += '<tr><th scope="col">prezime</th><td><input type="text" id="prezime"></td></tr>';
    output += '<tr><th scope="col">email</th><td><input type="text" id="email"></td></tr>';
    output += '<tr><th scope="col">OIB</th><td><input type="text" id="OIB"></td></tr>';
    output += '<tr><th scope="col">Ovlasti</th><td><input type="text" id="OVLASTI"></td></tr>';
    output += '<tr><th scope="col">Spol</th><td><input type="text" id="SPOL"></td></tr>';
    output += '<tr><th scope="col">godiste</th><td><input type="text" id="GODISTE"></td></tr></table>';
    output += '<button type="button" class="btn btn-warning" id="spremiKor">Spremi <i class="fas fa-save"></i></button> ';
    output += '<button type="button" class="btn btn-success" onclick="showKorisnici(' + page + ')">Odustani <i class="fas fa-window-close"></i></button>';
    $("#container").html(output);
}

function pagination(pageNmb, perPage, count) {
    //ne treba prikazivati ništa
    if (count < perPage) {
        return '';
    } else {
        var quotient = Math.ceil(count / perPage);
    }
    var next = pageNmb + 1;
    var prev = pageNmb - 1;
    var pagination = '<div class="float-right pagination">';

    //treba prikazati previous
    if (pageNmb > 1) {
        pagination += '<ul class="pagination"><li class="page-item "><a class="page-link" onclick="showKorisnici(' + prev + ')" href="javascript:void(0)">‹</a></li>';
    }

    for (i = pageNmb; i < pageNmb + 8; i++) {
        pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici(' + i + ')" href="javascript:void(0)">' + i + '</a></li>';
    }

    pagination += '<li class="page-item"><a class="page-link"  href="javascript:void(0)">...</a></li>';

    pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici(' + quotient + ')" href="javascript:void(0)">' + quotient + '</a></li>';

    pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici(' + next + ')" href="javascript:void(0)">›</a></li>';
    pagination += '</ul></div>';
    return pagination;
}




//LOV
function getSelectSpol(input,placeholder) {
    var output = '<select id="' + placeholder + '">'; 
    var items = {
        "opis":"spol..", 
        "0":"Muško",
        "1":"Žensko"
    }      
    output += generateOptions(input, items); 
    return output;   
}

var lovSpol = {
    "opis":"spol..", 
    "0":"Muško",
    "1":"Žensko"
}   
var lovOvlasti = {
    "opis":"Ovlasti korisnika", 
    "0":"Ništa",
    "1":"Samo pregled",
    "2":"Pregled i izmjena podataka",
    "3":"Administrator"
}  
var lovMP_VP = {
    "opis":"Maloprodaja ili Veleprodaja", 
    "0":"MP",
    "1":"VP"
}



function getSelect(input,placeholder, items) {
    var output = '<select id="' + placeholder + '">'; 
    output += generateOptions(input, items); 
    return output;   
}

function getSelectOvlasti(input, placeholder) {
    var output = '<select id="'+ placeholder + '">';
    var items = {
        "opis":"Ovlasti korisnika", 
        "0":"Ništa",
        "1":"Samo pregled",
        "2":"Pregled i izmjena podataka",
        "3":"Administrator"
    }      
    output += generateOptions(input, items); 
    return output;   
}



function selectFormBoravak(input) {
    var output = '<select id="BORAVAK">';
    var items = {
        "opis":"Trajanje boravka", 
        "3":"do 4 sata",
        "4":"do 10 sati"
    }      
    output += generateOptions(input, items); 
    return output;
}

function selectFormObrok(input) {
    var output = '<select id="OBROK">';
    var items = {
        "opis":"Broj obroka", 
        "0":"bez obroka",
        "1":"1 obrok",
        "2":"2 ili više obroka"
    }      
    output += generateOptions(input, items); 
    return output;
}


function generateOptions(input, items){
    var output = '';
    if(!input && input !=0){
        output += '<option disabled selected>' + items.opis + '</option>';  
    }else{
        output += '<option disabled>' + items.opis + '</option>'; 
    }
    for(var index in items) {
        if (index != 'opis'){
            if (index == input){
                output += '<option value='+ index + ' selected>' + items[index] + '</option>'; 
            }else{
                output += '<option value='+ index + '>' + items[index] + '</option>'; 
            }   
        }
    }
    output += '</select>';
    return output;
}

